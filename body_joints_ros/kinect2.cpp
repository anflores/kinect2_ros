#include "stdafx.h"
#include "kinect2.h"


kinect2::kinect2() : 
	m_hWnd(NULL),
	m_nStartTime(0),
	m_nLastCounter(0),
	m_nFramesSinceUpdate(0),
	m_fFreq(0),
	m_nNextStatusTime(0LL),
	m_pKinectSensor(NULL),
	m_pCoordinateMapper(NULL),
	m_pBodyFrameReader(NULL)

{
	initialized = false;
	

	for (int i = 0; i < BODY_COUNT; i++){
		trackedUsers[i] = false;
	}

}


kinect2::~kinect2()
{
}

HRESULT kinect2::init(){
	HRESULT hr;

	hr = GetDefaultKinectSensor(&m_pKinectSensor);
	if (FAILED(hr))
	{
		return hr;
	}

	if (m_pKinectSensor)
	{
		// Initialize the Kinect and get coordinate mapper and the body reader
		IBodyFrameSource* pBodyFrameSource = NULL;

		hr = m_pKinectSensor->Open();

		if (SUCCEEDED(hr))
		{
			hr = m_pKinectSensor->get_CoordinateMapper(&m_pCoordinateMapper);
		}

		if (SUCCEEDED(hr))
		{
			hr = m_pKinectSensor->get_BodyFrameSource(&pBodyFrameSource);
		}

		if (SUCCEEDED(hr))
		{
			hr = pBodyFrameSource->OpenReader(&m_pBodyFrameReader);
		}

		SafeRelease(pBodyFrameSource);
	}

	if (!m_pKinectSensor || FAILED(hr))
	{
		printf("No ready Kinect found!");
		return E_FAIL;
	}

	return hr;
}


void kinect2::update()
{
	if (!m_pBodyFrameReader)
	{
		
		return;
	}

	pBodyFrame = NULL;

	HRESULT hr = m_pBodyFrameReader->AcquireLatestFrame(&pBodyFrame);

	if (SUCCEEDED(hr))
	{

		nTime = 0;

		hr = pBodyFrame->get_RelativeTime(&nTime);

		ppBodies[BODY_COUNT] = { 0 };

		if (SUCCEEDED(hr))
		{
			hr = pBodyFrame->GetAndRefreshBodyData(_countof(ppBodies), ppBodies);
		}
		success = hr;
	}
}

bool kinect2::successed(){
	if (SUCCEEDED(success) && (m_pCoordinateMapper)){
		return true;
	}
		else{ return false; }
}


bool kinect2::ProcessBody(int i)
{
	bool body_tracked = false;
	HRESULT hr;
	IBody* pBody = ppBodies[i];
	if (pBody)
	{
		
		BOOLEAN bTracked = false;
		hr = pBody->get_IsTracked(&bTracked);
		showNewUser(bTracked, i);

		if (SUCCEEDED(hr) && bTracked)
		{

			body_tracked = true;
			pBody->get_HandLeftState(&leftHandState);
			pBody->get_HandRightState(&rightHandState);
			pBody->GetJoints(_countof(joints), joints);
			pBody->GetJointOrientations(_countof(joints_orientation), joints_orientation);
		}
	}
	return body_tracked;
}



void kinect2::endProcess(){
		
	if (!m_nStartTime)
	{
		m_nStartTime = nTime;
	}
	double fps = 0.0;

	LARGE_INTEGER qpcNow = { 0 };
	if (m_fFreq)
	{
		if (QueryPerformanceCounter(&qpcNow))
		{
			if (m_nLastCounter)
			{
				m_nFramesSinceUpdate++;
				fps = m_fFreq * m_nFramesSinceUpdate / double(qpcNow.QuadPart - m_nLastCounter);
			}
		}
	}
}

void kinect2::releaseBodies(){
	for (int i = 0; i < _countof(ppBodies); i++){
		SafeRelease(ppBodies[i]);
	}
	SafeRelease(pBodyFrame);
}



void kinect2::showNewUser(bool tracker, int i){
	if (trackedUsers[i] != tracker){
		if (tracker){
			printf("Note : New User %i found\n", i);
		}
		else{
			printf("Note : New User %i lost\n", i);
		}
		trackedUsers[i] = tracker;
	}
}
