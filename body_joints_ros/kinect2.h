#pragma once

#include <Kinect.h>
#include <cmath>
#include <cstdio>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <iostream>

#include <Windows.h>
#include <Ole2.h>

const int colorwidth = 1920;
const int colorheight = 1080;
const int width = 512;
const int height = 424;


class kinect2
{
public:
	/**********CONSTRUCTOR**************/
	kinect2();

	/**********DESTRUCTOR**************/
	~kinect2();


	/********** PARAMETERS ***********/
	bool initialized;                           // The kinect has been initialized
	BOOLEAN trackedUsers[BODY_COUNT];                            // Maps depth pixels to rgb pixels


	Joint joints[JointType_Count];				// List of joints in the tracked body
	JointOrientation joints_orientation[JointType_Count];
	HandState leftHandState = HandState_Unknown;
	HandState rightHandState = HandState_Unknown;



	IKinectSensor* sensor;                               // Kinect sensor
	IMultiSourceFrameReader* reader;                     // Kinect data source
	ICoordinateMapper* mapper;                           // Converts between depth, color, and 3d coordinates
	
	

	/*********** METHODS *********/
	HRESULT init();
	void getDepthData(IMultiSourceFrame* frame, GLubyte* dest);
	void getRgbData(IMultiSourceFrame* frame, GLubyte* dest);
	void getBodyData(IMultiSourceFrame* frame);
	void getKinectData();
	void update();
	bool successed();
	bool ProcessBody(int i);
	void endProcess();
	void releaseBodies();
	void showNewUser(bool tracker, int i);

	Joint getJoint_position(JointType type){ return joints[type]; }
	JointOrientation getJoint_orientation(JointType type){ return joints_orientation[type]; }



private:
	HWND                    m_hWnd;
	INT64                   m_nStartTime;
	INT64                   m_nLastCounter;
	double                  m_fFreq;
	INT64                   m_nNextStatusTime;
	DWORD                   m_nFramesSinceUpdate;

	// Current Kinect
	IKinectSensor*          m_pKinectSensor;
	ICoordinateMapper*      m_pCoordinateMapper;

	// Body reader
	IBodyFrameReader*       m_pBodyFrameReader;

	// For the body reader
	IBodyFrame* pBodyFrame;
	IBody* ppBodies[BODY_COUNT];
	INT64 nTime;
	HRESULT success;



};

