// body_joints_ros.cpp : Defines the entry point for the console application.
//
// rosserial_hello_world.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "stdafx.h"
#include <string>
#include <stdio.h>
#include "ros.h"
#include <ros/time.h>
#include <geometry_msgs\Twist.h>
#include <windows.h>
#include "parameters.h"
#include "kinect2.h"
#include <kinect.h>
#include <tf\FrameGraph.h>
#include <tf\tf.h>
#include <tf\tfMessage.h>
#include <tf\transform_broadcaster.h>
#include <string>

geometry_msgs::TransformStamped t, t2;
tf::TransformBroadcaster br;

char frame_id[] = "kinect2_tracker";


using std::string;






void publishjoint(int user, kinect2 kin, JointType type, char frame_id[], char child_id[], ros::NodeHandle nh)
{

	Joint joint = kin.getJoint_position(type);
	JointOrientation rot = kin.getJoint_orientation(type);

	float rotx_2 = pow(rot.Orientation.x, 2);
	float roty_2 = pow(rot.Orientation.y, 2);
	float rotz_2 = pow(rot.Orientation.z, 2);
	float rotw_2 = pow(rot.Orientation.w, 2);

	float norm = pow(rotx_2 + roty_2 + rotz_2 + rotw_2, 0.5);   // some norms for the rotation are 0, so avoid this transforms
	
	const char *one = "_";
	string s = std::to_string(user);
	char const *two = s.c_str();
	char result[100];

	strcpy_s(result, child_id); // copy string one into the result.
	strcat_s(result, one); // append string two to the result.
	strcat_s(result, two); // append string two to the result.



	t.header.stamp = nh.now();
	t.header.frame_id = frame_id;
	t.child_frame_id = result;
	t.transform.translation.x = joint.Position.X;
	t.transform.translation.z = joint.Position.Z;
	t.transform.translation.y = joint.Position.Y;
	
	
	if (norm == 1){



		t.transform.rotation.w = rot.Orientation.w;
		t.transform.rotation.x = rot.Orientation.x;
		t.transform.rotation.y = rot.Orientation.y;
		t.transform.rotation.z = rot.Orientation.z;
		br.sendTransform(t);

		t2.header.stamp = nh.now();
		t2.header.frame_id = "foo";
		t2.child_frame_id = frame_id;
		t2.transform.translation.x = 0;
		t2.transform.translation.y = 0;
		t2.transform.translation.z = 0;
		t2.transform.rotation.w = 0.5;
		t2.transform.rotation.x = 0.5;
		t2.transform.rotation.y = 0.5;
		t2.transform.rotation.z = 0.5;

		br.sendTransform(t2);
	}
	else if ((child_id == "Right_foot") || (child_id == "Left_foot") || (child_id == "Head")){
		t.transform.rotation.w = 1;
		t.transform.rotation.x = rot.Orientation.x;
		t.transform.rotation.y = rot.Orientation.y;
		t.transform.rotation.z = rot.Orientation.z;
		br.sendTransform(t);

		t2.header.stamp = nh.now();
		t2.header.frame_id = "foo";
		t2.child_frame_id = frame_id;
		t2.transform.translation.x = 0;
		t2.transform.translation.y = 0;
		t2.transform.translation.z = 0;
		t2.transform.rotation.w = 0.5;
		t2.transform.rotation.x = 0.5;
		t2.transform.rotation.y = 0.5;
		t2.transform.rotation.z = 0.5;

		br.sendTransform(t2);
	}


}


void publish_all_joints(kinect2 kin, char frame_id[], ros::NodeHandle nh, int user){
	

	publishjoint(user, kin, JointType_AnkleLeft,  frame_id,"Left_ankle", nh);
	publishjoint(user, kin, JointType_AnkleRight, frame_id, "Right_ankle", nh);

	publishjoint(user, kin, JointType_ElbowLeft, frame_id, "Left_elbow", nh);
	publishjoint(user, kin, JointType_ElbowRight, frame_id, "Right_elbow", nh);

	publishjoint(user, kin, JointType_FootLeft, frame_id, "Left_foot", nh);
	publishjoint(user, kin, JointType_FootRight, frame_id, "Right_foot", nh);

	publishjoint(user, kin, JointType_HandLeft, frame_id, "Left_hand", nh);
	publishjoint(user, kin, JointType_HandRight, frame_id, "Right_hand", nh);

	publishjoint(user, kin, JointType_HandTipLeft, frame_id, "Tip_left_hand", nh);
	publishjoint(user, kin, JointType_HandTipRight, frame_id, "Tip_right_hand", nh);

	publishjoint(user, kin, JointType_Head, frame_id, "Head", nh);
	publishjoint(user, kin, JointType_Neck, frame_id, "Neck", nh);

	publishjoint(user, kin, JointType_HipLeft, frame_id, "Left_hip", nh);
	publishjoint(user, kin, JointType_HipRight, frame_id, "Right_hip", nh);

	publishjoint(user, kin, JointType_KneeLeft, frame_id, "Left_knee", nh);
	publishjoint(user, kin, JointType_KneeRight, frame_id, "Right_knee", nh);

	publishjoint(user, kin, JointType_ShoulderLeft, frame_id, "Left_shoulder", nh);
	publishjoint(user, kin, JointType_ShoulderRight, frame_id, "Right_shoulder", nh);

	publishjoint(user, kin, JointType_SpineBase, frame_id, "Spine_base", nh);
	publishjoint(user, kin, JointType_SpineMid, frame_id, "Spine_mid", nh);
	publishjoint(user, kin, JointType_SpineShoulder, frame_id, "Spine_shoulder", nh);

	publishjoint(user, kin, JointType_ThumbLeft, frame_id, "Left_thumb", nh);
	publishjoint(user, kin, JointType_ThumbRight, frame_id, "Right_thumb", nh);

	publishjoint(user, kin, JointType_WristLeft, frame_id, "Left_wrist", nh);
	publishjoint(user, kin, JointType_WristRight, frame_id, "Right_wrist", nh);
}


int _tmain(int argc, _TCHAR* argv[])
{

	kinect2 kin;
	kin.init();
	

	ros::NodeHandle nh;
	char * ros_master = ROS_MASTER;
	

	printf("Connecting to server at %s\n", ros_master);
	nh.initNode(ros_master);
	br.init(nh);

	string frame("kinect_joints");

	printf("Main loop\n");

	int i = 0;
	while (true){
		//std::cout << "iteration " << i << std::endl;
		++i;

		kin.update();
		if (kin.successed())
		{
			bool tracked_body;
			for (int user = 0; user < 6; user++){

				tracked_body = kin.ProcessBody(user);
				if (tracked_body){
					publish_all_joints(kin, frame_id, nh, user);
				}
			}
			kin.endProcess();
		}

		kin.releaseBodies();

		nh.spinOnce();
		Sleep(10);
	}

	printf("All done!\n");
	return 0;
}


