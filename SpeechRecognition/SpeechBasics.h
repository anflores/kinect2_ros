#pragma once
#include "Resource.h"
#include "KinectAudioStream.h"
#include <uuids.h>
#undef ERROR 
#include <ros.h>
#include <std_msgs\String.h>
#include <iri_command_tracker\speech_command.h>



// For speech APIs
// NOTE: To ensure that application compiles and links against correct SAPI versions (from Microsoft Speech
//       SDK), VC++ include and library paths should be configured to list appropriate paths within Microsoft
//       Speech SDK installation directory before listing the default system include and library directories,
//       which might contain a version of SAPI that is not appropriate for use together with Kinect sensor.
#include <sapi.h>
__pragma(warning(push))
__pragma(warning(disable:6385 6001)) // Suppress warnings in public SDK header
#include <sphelper.h>
__pragma(warning(pop))





class CSpeechBasics
{
public:
	CSpeechBasics();
	~CSpeechBasics();

private:
	ros::NodeHandle nh;
	iri_command_tracker::speech_command cmd;
	ros::Publisher* pub;


	// Main application dialog window
	HWND                    m_hWnd;

	static LPCWSTR          GrammarFileName;
	// Current Kinect sensor
	IKinectSensor*          m_pKinectSensor;       // Current Kinect sensor
	IAudioBeam*             m_pAudioBeam;          // A single audio beam off the Kinect sensor.
	IStream*                m_pAudioStream;        // An IStream derived from the audio beam, used to read audio samples
	KinectAudioStream*      m_p16BitAudioStream;   // Stream for converting 32bit Audio provided by Kinect to 16bit required by speeck
	WAITABLE_HANDLE         m_hSensorNotification; // Handle for sensor notifications
	ISpStream*              m_pSpeechStream;       // Stream given to speech recognition engine
	ISpRecognizer*          m_pSpeechRecognizer;   // Speech recognizer
	ISpRecoContext*         m_pSpeechContext;      // Speech recognizer context
	ISpRecoGrammar*         m_pSpeechGrammar;      // Speech grammar
	HANDLE                  m_hSpeechEvent;        // Event triggered when we detect speech recognition




public:
	//***************** FUNCTIONS ***************//
	void connectRos(char* ip);
	int                     Run();
	HRESULT                 StartKinect();
	HRESULT                 InitializeSpeech();
	HRESULT                 CreateSpeechRecognizer();
	HRESULT                 LoadSpeechGrammar();
	HRESULT                 StartSpeechRecognition();
	void                    ProcessSpeech();
	void                    SetStatusMessage(char* szMessage);
	LRESULT CALLBACK        DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK MessageRouter(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	char* MapSpeechTagToAction(LPCWSTR pszSpeechTag);
};

