// SpeechRecognition.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <iostream>
#undef ERROR 
#include <ros.h>
#include <std_msgs\String.h>
#include <std_msgs\Float32.h>



#include "SpeechBasics.h"
#define ROS_MASTER "192.168.100.106";            // IP address of the master ROS (run in a Ubuntu machine in the same LAN Network)


LPCWSTR CSpeechBasics::GrammarFileName = L"SpeechBasics-D2D.grxml";
// MAIN ENTRY FUNCTION


int _tmain(int argc, _TCHAR* argv[])
{
	char * ros_master = ROS_MASTER;
	CSpeechBasics speech;
	speech.connectRos(ros_master);
	speech.StartKinect();
	speech.Run();


	return 0;
}

