#include "stdafx.h"
#include "SpeechBasics.h"
#include <iostream>


CSpeechBasics::CSpeechBasics() :   
m_pKinectSensor(NULL),
m_pAudioBeam(NULL),
m_pAudioStream(NULL),
m_p16BitAudioStream(NULL),
m_hSensorNotification(reinterpret_cast<WAITABLE_HANDLE>(INVALID_HANDLE_VALUE)),
m_pSpeechStream(NULL),
m_pSpeechRecognizer(NULL),
m_pSpeechContext(NULL),
m_pSpeechGrammar(NULL),
m_hSpeechEvent(INVALID_HANDLE_VALUE)
{
	
}


CSpeechBasics::~CSpeechBasics()
{
	if (m_pKinectSensor)
	{
		m_pKinectSensor->Close();
	}

	//16 bit Audio Stream
	if (NULL != m_p16BitAudioStream)
	{
		delete m_p16BitAudioStream;
		m_p16BitAudioStream = NULL;
	}
	SafeRelease(m_pAudioStream);
	SafeRelease(m_pAudioBeam);
	SafeRelease(m_pKinectSensor);
}

HRESULT CSpeechBasics::StartKinect(){
	HRESULT hr = S_OK;
	IAudioSource* pAudioSource = NULL;
	IAudioBeamList* pAudioBeamList = NULL;
	BOOLEAN sensorState = TRUE;

	hr = GetDefaultKinectSensor(&m_pKinectSensor);
	if (FAILED(hr))
	{
		SetStatusMessage("Failed getting default sensor!");
		return hr;
	}

	hr = m_pKinectSensor->SubscribeIsAvailableChanged(&m_hSensorNotification);

	if (SUCCEEDED(hr))
	{
		hr = m_pKinectSensor->Open();
	}

	if (SUCCEEDED(hr))
	{
		hr = m_pKinectSensor->get_AudioSource(&pAudioSource);
	}

	if (SUCCEEDED(hr))
	{
		hr = pAudioSource->get_AudioBeams(&pAudioBeamList);
	}

	if (SUCCEEDED(hr))
	{
		hr = pAudioBeamList->OpenAudioBeam(0, &m_pAudioBeam);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_pAudioBeam->OpenInputStream(&m_pAudioStream);
		m_p16BitAudioStream = new KinectAudioStream(m_pAudioStream);
	}

	if (FAILED(hr))
	{
		SetStatusMessage("Failed opening an audio stream!");
	}

	SafeRelease(pAudioBeamList);
	SafeRelease(pAudioSource);
	return hr;
}




void CSpeechBasics::connectRos(char* ip){
	printf("Connecting to server at %s\n", ip);
	nh.initNode(ip);
	pub = new ros::Publisher("command", &cmd);
	nh.advertise(*pub);
}


HRESULT CSpeechBasics::InitializeSpeech()
{
	// Audio Format for Speech Processing
	WORD AudioFormat = WAVE_FORMAT_PCM;
	WORD AudioChannels = 1;
	DWORD AudioSamplesPerSecond = 16000;
	DWORD AudioAverageBytesPerSecond = 32000;
	WORD AudioBlockAlign = 2;
	WORD AudioBitsPerSample = 16;

	WAVEFORMATEX wfxOut = { AudioFormat, AudioChannels, AudioSamplesPerSecond, AudioAverageBytesPerSecond, AudioBlockAlign, AudioBitsPerSample, 0 };
	if (FAILED(::CoInitialize(NULL)))
		return FALSE;

	HRESULT hr = CoCreateInstance(CLSID_SpStream, NULL, CLSCTX_INPROC_SERVER, __uuidof(ISpStream), (void**)&m_pSpeechStream);


	if (SUCCEEDED(hr))
	{

		m_p16BitAudioStream->SetSpeechState(true);
		hr = m_pSpeechStream->SetBaseStream(m_p16BitAudioStream, SPDFID_WaveFormatEx, &wfxOut);
	}

	if (SUCCEEDED(hr))
	{
		hr = CreateSpeechRecognizer();
	}

	if (FAILED(hr))
	{
		SetStatusMessage("Could not create speech recognizer. Please ensure that Microsoft Speech SDK and other sample requirements are installed.");
		return hr;
	}

	hr = LoadSpeechGrammar();

	if (FAILED(hr))
	{
		SetStatusMessage("Could not load speech grammar. Please ensure that grammar configuration file was properly deployed.");
		return hr;
	}

	hr = StartSpeechRecognition();

	if (FAILED(hr))
	{
		SetStatusMessage("Could not start recognizing speech.");
		return hr;
	}

	return hr;
}



HRESULT CSpeechBasics::CreateSpeechRecognizer()
{
	ISpObjectToken *pEngineToken = NULL;

	HRESULT hr = CoCreateInstance(CLSID_SpInprocRecognizer, NULL, CLSCTX_INPROC_SERVER, __uuidof(ISpRecognizer), (void**)&m_pSpeechRecognizer);
	
	if (SUCCEEDED(hr))
	{
		m_pSpeechRecognizer->SetInput(m_pSpeechStream, TRUE);

		// If this fails here, you have not installed the acoustic models for Kinect
		hr = SpFindBestToken(SPCAT_RECOGNIZERS, L"Language=409;Kinect=True", NULL, &pEngineToken);

		if (SUCCEEDED(hr))
		{
			m_pSpeechRecognizer->SetRecognizer(pEngineToken);
			hr = m_pSpeechRecognizer->CreateRecoContext(&m_pSpeechContext);

			// For long recognition sessions (a few hours or more), it may be beneficial to turn off adaptation of the acoustic model. 
			// This will prevent recognition accuracy from degrading over time.
			if (SUCCEEDED(hr))
			{
				hr = m_pSpeechRecognizer->SetPropertyNum(L"AdaptationOn", 0);
			}
		}
	}
	SafeRelease(pEngineToken);
	return hr;
}


HRESULT CSpeechBasics::LoadSpeechGrammar()
{
	HRESULT hr = m_pSpeechContext->CreateGrammar(1, &m_pSpeechGrammar);

	if (SUCCEEDED(hr))
	{
		// Populate recognition grammar from file
		hr = m_pSpeechGrammar->LoadCmdFromFile(GrammarFileName, SPLO_STATIC);
	}

	return hr;
}



HRESULT CSpeechBasics::StartSpeechRecognition()
{
	HRESULT hr = S_OK;

	// Specify that all top level rules in grammar are now active
	hr = m_pSpeechGrammar->SetRuleState(NULL, NULL, SPRS_ACTIVE);
	if (FAILED(hr))
	{
		return hr;
	}

	// Specify that engine should always be reading audio
	hr = m_pSpeechRecognizer->SetRecoState(SPRST_ACTIVE_ALWAYS);
	if (FAILED(hr))
	{
		return hr;
	}

	// Specify that we're only interested in receiving recognition events
	hr = m_pSpeechContext->SetInterest(SPFEI(SPEI_RECOGNITION), SPFEI(SPEI_RECOGNITION));
	if (FAILED(hr))
	{
		return hr;
	}

	// Ensure that engine is recognizing speech and not in paused state
	hr = m_pSpeechContext->Resume(0);
	if (FAILED(hr))
	{
		return hr;
	}

	m_hSpeechEvent = m_pSpeechContext->GetNotifyEventHandle();
	return hr;
}



void CSpeechBasics::ProcessSpeech()
{
	const float ConfidenceThreshold = 0.2f;

	SPEVENT curEvent = { SPEI_UNDEFINED, SPET_LPARAM_IS_UNDEFINED, 0, 0, 0, 0 };
	ULONG fetched = 0;
	HRESULT hr = S_OK;

	float fBeamAngle = 0.f;
	float fBeamAngleConfidence = 0.f;

	m_pSpeechContext->GetEvents(1, &curEvent, &fetched);
	while (fetched > 0)
	{
		switch (curEvent.eEventId)
		{
		case SPEI_RECOGNITION:
			if (SPET_LPARAM_IS_OBJECT == curEvent.elParamType)
			{
				// this is an ISpRecoResult
				ISpRecoResult* result = reinterpret_cast<ISpRecoResult*>(curEvent.lParam);
				SPPHRASE* pPhrase = NULL;

				hr = result->GetPhrase(&pPhrase);
				if (SUCCEEDED(hr))
				{
					if ((pPhrase->pProperties != NULL) && (pPhrase->pProperties->pFirstChild != NULL))
					{
						const SPPHRASEPROPERTY* pSemanticTag = pPhrase->pProperties->pFirstChild;
						if (pSemanticTag->SREngineConfidence > ConfidenceThreshold)
						{
							char* action = MapSpeechTagToAction(pSemanticTag->pszValue);
							

							m_pAudioBeam->get_BeamAngle(&fBeamAngle);
							m_pAudioBeam->get_BeamAngleConfidence(&fBeamAngleConfidence);
							double fBeamDegrees = 180.0f * fBeamAngle / static_cast<float>(M_PI);

							std::cout << action << "    angle: " << fBeamDegrees << " confidence: " << fBeamAngleConfidence << std::endl;
							cmd.command = action;
							cmd.angle = fBeamDegrees;
							cmd.confidence = fBeamAngleConfidence;
							pub->publish(&cmd);




							
							


						}
					}
					::CoTaskMemFree(pPhrase);
				}
			}
			break;
		}

		m_pSpeechContext->GetEvents(1, &curEvent, &fetched);
	}
	

	nh.spinOnce();

}


int CSpeechBasics::Run()
{
	MSG       msg = { 0 };
	WNDCLASS  wc;
	const int maxEventCount = 2;
	int eventCount = 1;
	HANDLE hEvents[maxEventCount];

	// Dialog custom window class
	ZeroMemory(&wc, sizeof(wc));
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.cbWndExtra = DLGWINDOWEXTRA;
	wc.hCursor = LoadCursorW(NULL, IDC_ARROW);
	wc.lpfnWndProc = DefDlgProcW;
	wc.lpszClassName = L"SpeechBasicsAppDlgWndClass";

	if (!RegisterClassW(&wc))
	{
		return 0;
	}


	// Main message loop
	while (WM_QUIT != msg.message)
	{
		if (m_hSpeechEvent != INVALID_HANDLE_VALUE)
		{
			hEvents[1] = m_hSpeechEvent;
			eventCount = 2;
		}

		hEvents[0] = reinterpret_cast<HANDLE>(m_hSensorNotification);

		// Check to see if we have either a message (by passing in QS_ALLINPUT)
		// Or sensor notification (hEvents[0])
		// Or a speech event (hEvents[1])
		DWORD waitResult = MsgWaitForMultipleObjectsEx(eventCount, hEvents, 50, QS_ALLINPUT, MWMO_INPUTAVAILABLE);

		switch (waitResult)
		{
		case WAIT_OBJECT_0:
		{
			BOOLEAN sensorState = FALSE;

			// Getting the event data will reset the event.
			IIsAvailableChangedEventArgs* pEventData = nullptr;
			if (FAILED(m_pKinectSensor->GetIsAvailableChangedEventData(m_hSensorNotification, &pEventData)))
			{
				SetStatusMessage("Failed to get sensor availability.");
				break;
			}

			pEventData->get_IsAvailable(&sensorState);
			SafeRelease(pEventData);

			if (sensorState == FALSE)
			{
				SetStatusMessage("Sensor has been disconnected - attach Sensor");
			}
			else
			{
				HRESULT hr = S_OK;

				if (m_pSpeechRecognizer == NULL)
				{
					hr = InitializeSpeech();
				}
				if (SUCCEEDED(hr))
				{
					SetStatusMessage("Ready to Speech Recognition");
				}
				else
				{
					SetStatusMessage("Speech Initialization Failed");
				}
			}
		}
		break;
		case WAIT_OBJECT_0 + 1:
			if (eventCount == 2)
			{
				ProcessSpeech();
			}
			break;
		}

		while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// If a dialog message will be taken care of by the dialog proc

			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}
	}
	return 0;
}



LRESULT CALLBACK CSpeechBasics::DlgProc(HWND hWnd, UINT message, WPARAM /* wParam */, LPARAM /* lParam */)
{
	LRESULT result = FALSE;

	switch (message)
	{
	case WM_INITDIALOG:
	{
		// Bind application window handle
		m_hWnd = hWnd;


		// Look for a connected Kinect, and create it if found
		HRESULT hr = StartKinect();
		if (FAILED(hr))
		{
			break;
		}

		SetStatusMessage("Waiting for Sensor and Speech Initialization - Please ensure Sensor is attached.");
		result = FALSE;
		break;
	}


	// If the titlebar X is clicked, destroy app
	case WM_CLOSE:
		if (NULL != m_p16BitAudioStream)
		{
			m_p16BitAudioStream->SetSpeechState(false);
		}

		if (NULL != m_pSpeechRecognizer)
		{
			m_pSpeechRecognizer->SetRecoState(SPRST_INACTIVE_WITH_PURGE);

			//cleanup here
			SafeRelease(m_pSpeechStream);
			SafeRelease(m_pSpeechRecognizer);
			SafeRelease(m_pSpeechContext);
			SafeRelease(m_pSpeechGrammar);
		}

		DestroyWindow(hWnd);
		result = TRUE;
		break;

	case WM_DESTROY:
		// Quit the main message pump
		PostQuitMessage(0);
		result = TRUE;
		break;
	}
	return result;
}


LRESULT CALLBACK CSpeechBasics::MessageRouter(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	CSpeechBasics* pThis = NULL;

	if (WM_INITDIALOG == uMsg)
	{
		pThis = reinterpret_cast<CSpeechBasics*>(lParam);
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis));
	}
	else
	{
		pThis = reinterpret_cast<CSpeechBasics*>(::GetWindowLongPtr(hWnd, GWLP_USERDATA));
	}

	if (NULL != pThis)
	{
		return pThis->DlgProc(hWnd, uMsg, wParam, lParam);
	}

	return 0;
}



char* CSpeechBasics::MapSpeechTagToAction(LPCWSTR pszSpeechTag)
{
	struct SpeechTagToAction
	{
		LPCWSTR pszSpeechTag;
		char* action;
	};
	const SpeechTagToAction Map[] =
	{
		{ L"NO",       "no" },
		{ L"STOP",     "stop" },
		{ L"FORWARD",  "forward" },
		{ L"BACKWARD", "back" },
		{ L"LEFT",     "left" },
		{ L"RIGHT",    "right" },
		{ L"UP",       "up" },
		{ L"DOWN",     "down" },
		{ L"MORE",     "more" },
		{ L"TAKE",     "take" },
		{ L"DROP",     "drop" },
		{ L"GRAB",     "grab" },
		{ L"MOVE",     "move" },
		{ L"LEAVE",    "leave" },
		{ L"DRESS",    "dress" },
		{ L"ABORT",    "abort" },
		{ L"RED", "red" },
		{ L"YELLOW", "yellow" },
		{ L"GREEN", "green" },
		{ L"BLUE", "blue" },
		{ L"START", "start" }


	};
	char* action;

	for (int i = 0; i < _countof(Map); ++i)
	{
		if ((Map[i].pszSpeechTag != NULL) && (0 == wcscmp(Map[i].pszSpeechTag, pszSpeechTag)))
		{
			action = Map[i].action;
			break;
		}
	}

	return action;
}




void CSpeechBasics::SetStatusMessage(char* szMessage)
{
	std::cout << szMessage << std::endl;
}