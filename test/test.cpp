// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ros.h"
#include <std_msgs/String.h>
#define ROS_MASTER "192.168.100.106";            // IP address of the master ROS (run in a Ubuntu machine in the same LAN Network)

#include <windows.h>


ros::NodeHandle nh;
std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);
char hello[13] = "hello world!";
int _tmain(int argc, _TCHAR* argv[])
{
	char * ros_master = ROS_MASTER;


	printf("Connecting to server at %s\n", ros_master);
	nh.initNode(ros_master);
	nh.advertise(chatter);

	while (true){
		str_msg.data = hello;
		chatter.publish(&str_msg);
		nh.spinOnce();
		Sleep(1000);
	}
}

